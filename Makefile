VERSION="2021-01-02"
docker:
	docker build -t pkoperek/baselinesme:latest -t pkoperek/baselinesme:${VERSION} .

docker-push: docker
	docker push pkoperek/baselinesme:${VERSION}
	docker push pkoperek/baselinesme:latest
